# Movie Recommendation System with Spark MLlib #

* Dataset is from the MovieLens stable benchmark rating dataset: http://grouplens.org/datasets/movielens/20m/

* Databricks platform community edition is used in this project: https://databricks.com/try-databricks